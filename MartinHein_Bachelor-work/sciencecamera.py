"""
CTA Science camera class.
"""

import uuid
import logging
import numpy as np
import astropy.units as u

logging.getLogger(__name__)


class ScienceCamera:
    """
    CTA Science camera class.
    """

    def __init__(self, name="NoneCamera"):

        self.uuid = uuid.uuid4()
        self.name = name
        self.focal_length = None
        self.rotation = None
        
        ## @Martin Hein
        self.tilt = None # TODO: not implemented in CameraFrame
        self.led_radius = None # or 0 * u.m
        self.led_positions = None
        self.led_intensity = None # or 0
        self.vertex_positions = None
        self.lighted_intensity = None # or 0


class FlashCam(ScienceCamera):
    def __init__(self, name="FlashCam"):

        ScienceCamera.__init__(self, name=name)

        # LED positions private communication GP (12.7.2021),
        # as viewed from the backside of the camera
        self.led_positions = (
            np.array(
                [
                    [1267.30, -339.70],
                    [927.70, -927.70],
                    [656.00, -1136.20],
                    [-656.00, -1136.20],
                    [-927.70, -927.70],
                    [-1267.30, -339.70],
                    [-1267.30, 339.70],
                    [-927.70, 927.70],
                    [-656.00, 1136.20],
                    [656.00, 1136.20],
                    [927.70, 927.70],
                    [1267.30, 339.70],
                ]
            )
            * u.mm
        )

        self.rotation = 0.0 * u.deg
        self.focal_length = 16.0 * u.m


class MAGICCam(ScienceCamera):
    def __init__(self, name="MAGICCam"):

        ScienceCamera.__init__(self, name=name)

        self.rotation = 0.0 * u.deg
        self.focal_length = 17 * u.m
        self.tilt = [0.0, 0.0] * u.deg

        ## @Martin Hein
        self.led_positions = self._construct_leds_on_circle()
        self.led_radius = 5e-3 * u.m # TODO: t.b.d.
        self.led_intensity = (
            np.array(
                [
                    60000,
                    60000,
                    60000,
                    60000,
                    60000,
                    60000,
                    60000,
                    60000,
                    60000,
                    60000,
                    60000,
                    60000
                ]
            )
        )# can be set as liked
        self.n_rays = 1000 # for gaussian profile
        
        ## @Martin Hein
        height, width = (2.0, 2.3)
        self.vertex_positions = ( # correct order! (x:down, y:left)
            np.array( # here clockwise, ccw also possible
                [
                    [-width/2, -height/2],
                    [width/2, -height/2],
                    [width/2, height/2],
                    [-width/2, height/2],

                    ## hardcoded before
                    # [-1.13, -1.03], # top right

                    # # additional structure on top right corner
                    # [-0.95, -1.03], # top left
                    # [-0.95, -1.25], # top right
                    # [-0.55, -1.25], # bottom right
                    # [-0.55, -1.0], # bottom left

                    # [1.17, -1.0], # bottom right
                    # [1.2, 0.97], # bottom left
                    # [-1.12, 1.0] # top left
                ]
            )
            * u.m
        )

        self.lighted_intensity = 1000

    def _construct_leds_on_circle(self):
        """
        Helper function to construct (approximately) the position of the
        MAGIC LEDs in the science camera frame
        """

        radius = 0.63 * u.m  # approximate LED circle radius (from images)
        position_angles = np.linspace(0, 330, 12) * u.deg

        x = (-radius * np.cos(position_angles)).reshape(-1, 1)
        y = (radius * np.sin(position_angles)).reshape(-1, 1)

        return np.concatenate((x, y), axis=1)
