import logging
import time
import numpy as np
from skimage.draw import polygon

from astropy import units as u
from astropy import constants as const
from astropy.coordinates import SkyCoord, AltAz

from scipy.interpolate import griddata

from ctapipe.coordinates import CameraFrame
from ..coordinates import SkyCameraFrame

from .exposure import Exposure
from .moonlight import MoonlightMap
from ..coordinates.utils import rotate_from_pole
from ..catalog import query_catalog


MIN_MAG = -12.0  # minimum applicable magnitude
MAX_MAG = 9.0  # maximum applicable magnitude

DURATION = 10  # standard exposure time (s)
TIMESTEPS = 20  # number of time steps for exposure

SMEARING_RADIUS = 3.5  # spot smearing radius (pixels)
SMEARING_RAYS = 200  # number of spot smearing rays

APPLY_MOONLIGHT = True  # apply moonlight
APPLY_NOISE = True  # apply camera noise

WAVELENGTH = 550  # wavelength of star light (nm)

log = logging.getLogger(__name__)


class ExposureSimulator:
    def __init__(self):
        pass

    def process(self, camera, telescope_pointing, start_time, duration, scienceCamera=None, **kwargs):

        if not telescope_pointing.isscalar:
            log.error("process() can only deal with single telescope pointings.")
            return None

        if np.isnan(telescope_pointing.ra) or np.isnan(telescope_pointing.dec):
            log.error(f"{telescope_pointing} is not a valid coordinate.")
            return None

        log.info(f"simulating image...")
        log.info(f"pointing {telescope_pointing}")
        log.info(f"camera {camera.__repr__()}")
        log.info(f"exposure start: {start_time}, duration: {duration}")

        start = time.perf_counter()

        # create exposure
        self.exposure = Exposure(is_simulated=True)
        self.exposure.camera = camera
        self.exposure.camera_configname = camera.name

        # simulation settings
        siminfo = self.exposure.simulation_info
        siminfo.time_step = kwargs.get("timestep", duration / TIMESTEPS)
        siminfo.min_mag = kwargs.get("min_mag", MIN_MAG)
        siminfo.max_mag = kwargs.get("max_mag", MAX_MAG)

        siminfo.smearing_radius = kwargs.get("smearing_radius", SMEARING_RADIUS)
        siminfo.smearing_rays = kwargs.get("smearing_rays", SMEARING_RAYS)

        siminfo.has_noise = kwargs.get("apply_noise", APPLY_NOISE)
        siminfo.has_moonlight = kwargs.get("apply_moonlight", APPLY_MOONLIGHT)

        siminfo.fov = kwargs.get("fov", camera.fov.to(u.deg))
        siminfo.coords_radec = telescope_pointing

        # exposure and camera settings
        self.exposure.start_time = start_time
        self.exposure.duration = duration
        self.exposure.telescope_pointing = telescope_pointing
        self.exposure.nominal_telescope_pointing = telescope_pointing
        self.exposure.pressure = kwargs.get("pressure", None)
        self.exposure.temperature = kwargs.get("temperature", None)

        # generate list of stars in the region
        c, mag = query_catalog(
            fov_centre=self.exposure.camera_pointing,
            fov_radius=1.5 * np.max(siminfo.fov) / 2,
            min_mag=siminfo.min_mag,
            max_mag=siminfo.max_mag,
        )
        siminfo.coords_radec = c
        siminfo.magnitudes = mag

        # calculate true star positions in AltAz and in the CCD chip. These are the ones at
        # the time of half the duration of the exposure.
        siminfo.coords_altaz_meanexp = siminfo.coords_radec.transform_to(
            self.exposure.altazframe
        )
        siminfo.coords_chip_meanexp = siminfo.coords_radec.transform_to(
            self.exposure.skycameraframe
        )
        siminfo.coords_pix_meanexp = camera.transform_to_pixels(
            siminfo.coords_chip_meanexp
        )

        # now render the image
        # take into account field rotation during the exposure
        # for this we render the image at different time steps and sum up the pixel contents

        # transform RADec position of stars to AltAz
        n_times = int(self.exposure.duration / siminfo.time_step)
        obstime = (
            self.exposure.start_time
            + np.linspace(0, 1, n_times) * self.exposure.duration
        )

        # create altaz systems for all time steps simultaneously
        altaz_sys = AltAz(location=camera.location, obstime=obstime[:, np.newaxis])

        # transform star position coordinates. Make use of astropy broadcasting that
        # creates a SkyCoord object of shape (n_times, n_stars)
        coords_altaz = siminfo.coords_radec.transform_to(altaz_sys)

        # determine stellar photon flux from the visual magnitudes
        wavelength = WAVELENGTH * u.nm  # somewhere in the blue-green (?)
        aperture = camera.aperture

        # conversion of magnitude to flux taken from Wikipedia
        # (probably wrong)
        # apply magnitude +0.25 correction, obtained from comparing to real images
        energy_flux = (
            np.power(10, -0.4 * (siminfo.magnitudes / u.mag + 19 + 0.25))
            * u.W
            / u.m ** 2
        )
        photon_energy = const.h * const.c / wavelength

        # calculate number of photons detected by CCD chip
        photon_weights = (
            energy_flux
            * aperture
            / photon_energy
            * siminfo.time_step
            * camera.efficiency
        ).decompose()

        # Gaussian smearing: smear the true position on the chip by transforming many (Gaussian-distributed)
        # rays from altaz into the chip.
        # We smear by the angular resolution that corresponds to a given
        # confusion circle in pixels on the chip
        if not np.isclose(siminfo.smearing_radius, 0.0):

            n_stars = len(siminfo.magnitudes)

            r_smear = np.arctan(
                siminfo.smearing_radius * camera.pixel_size / camera.focal_length[0]
            ).to("arcsec")
            log.info(
                "smearing star positions by Gaussian of width {:.0f}".format(r_smear)
            )

            # use 2D Gaussian (polar coordinates) as smearing kernel
            radial_offset = (
                np.sqrt(
                    np.random.exponential(
                        r_smear.value ** 2,
                        size=(siminfo.smearing_rays, n_times, n_stars),
                    )
                )
                * u.arcsec
            )
            position_angle = (
                np.random.random(size=(siminfo.smearing_rays, n_times, n_stars))
                * 360
                * u.deg
            )

            # smear coordinates in the altaz system
            # this results in a coordinate array of shape (n_times, n_beams*n_stars)
            # with correctly ordered elements
            coords_smeared = np.swapaxes(
                coords_altaz.directional_offset_by(position_angle, radial_offset), 0, 1
            ).reshape(n_times, -1)

            # distribute photon flux among all smeared light rays
            photon_weights /= siminfo.smearing_rays
            photon_weights = np.tile(photon_weights, siminfo.smearing_rays)

        else:
            coords_smeared = coords_altaz

        log.info("rendering image...")

        first = True
        for i in range(n_times):
            # project star positions into PointingCamera
            # select those (pre-transformed) altaz positions that belong
            # to the given time step.
            # Note: handling each time step separately is neccessary because
            # the project_into() method assumes (for performance reasons)
            # that all AltAz coordinates have the same time.
            #
            # TODO: check for horizon

            coords_ccd_smeared = self.exposure.camera.project_into(
                coords_smeared[i], telescope_pointing=self.exposure.telescope_pointing
            )

            # project into pixel system and clip to chip boundaries
            coords_pix_smeared = camera.transform_to_pixels(coords_ccd_smeared)
            clip_mask_smeared = camera.clip_to_chip(coords_pix_smeared)

            # add up all coordinates to common vector
            if first:
                coords_pix_smeared_aggregated = coords_pix_smeared[clip_mask_smeared]
                photon_weights_aggregated = photon_weights[clip_mask_smeared]
                first = False
            else:
                coords_pix_smeared_aggregated = np.append(
                    coords_pix_smeared_aggregated,
                    coords_pix_smeared[clip_mask_smeared],
                    axis=0,
                )
                photon_weights_aggregated = np.append(
                    photon_weights_aggregated, photon_weights[clip_mask_smeared]
                )

            log.debug("\tprocessed {}/{} time steps".format(i + 1, n_times))

        # to fill the image, create the image as a 2D histogram with
        # proper number of pixels, then fill each star coordinate with the
        # proper photon flux weight.
        # take care that FITS/astropy convention is followed, i.e. numpy
        # index 0 covers physical pixel 1 which covers the range [-0.5, 0.5]
        nx, ny = camera.num_pix
        image, _ = np.histogramdd(
            coords_pix_smeared_aggregated,
            bins=(nx, ny),
            weights=photon_weights_aggregated,
            range=((-0.5, nx - 0.5), (-0.5, ny - 0.5)),
        )

        # apply moonlight
        if siminfo.has_moonlight:
            log.info("applying moonlight")
            image += self.estimate_moonlight()

        # apply image noise
        if siminfo.has_noise:
            log.info("applying noise")

            try:
                noise_image = np.random.normal(
                    loc=self.exposure.camera.noise_mean,
                    scale=self.exposure.camera.noise_rms,
                    size=image.shape,
                )
                image += noise_image
            except:
                log.warning("applying image noise failed.")
        
        ## @Martin Hein
        ## simulate science camera body 
        if scienceCamera is not None:
            log.info("constructing science camera structure")
            self.simulate_cameraBody(scienceCamera, self.exposure, image)

        # store as image with proper bit depth
        dtype = camera.dtype
        if camera.dtype is None:
            log.warn(f"no bit depth given for camera {camera.name}. Assuming 16 bit.")
            dtype = np.uint16
        self.exposure.image = image.astype(dtype)

        stop = time.perf_counter()
        log.info(f"simulation finished ({(stop-start):.2f} seconds).")

        return self.exposure

    def estimate_moonlight(self):
        """
        Produce a moon-light map: construct homogeneously distributed coordinates in a FoV-sized cone,
        calculate expected moon brightness and transform into the image
        :returns: image of moonlight in counts/pixel
        :rtype: array
        """

        camera = self.exposure.camera

        # dice points homogeneously distributed in the FoV cone
        fov = 1.1 * np.sqrt(camera.fov[0] ** 2 + camera.fov[1] ** 2)
        n = 10000

        # use polar coordinates as approximation of spherical coordinates
        r = np.sqrt(np.random.rand(n) * (fov / 2) ** 2)
        phi = np.random.rand(n) * 360.0 * u.deg

        # point distribution at the pole of the coordinate system, rotate to
        # observation position, and project into chip

        camera_centre_altaz = self.exposure.camera_pointing.transform_to(
            self.exposure.altazframe
        )

        b_coords = SkyCoord(alt=90 * u.deg - r, az=phi, frame=self.exposure.altazframe)
        b_coords_rot = rotate_from_pole(b_coords, camera_centre_altaz)
        b_coords_pix = camera.transform_to_pixels(
            self.exposure.transform_to_camera(b_coords_rot)
        )
        b_mask = camera.clip_to_chip(b_coords_pix)

        log.info("\tusing FoV of {:.1f}".format(fov.to("deg")))
        log.info("\tusing {} test coordinates".format(n))
        log.info(
            "\thit efficiency is {:.1f}%".format(np.count_nonzero(b_mask) / n * 100)
        )

        # calculate moonlight brightness at these coordinates
        m = MoonlightMap()
        b_fluxes, _, _, _ = m.process(b_coords_rot)

        # calculate observed photons from fluxes and interpolate
        solid_angle = camera.pixel_solid_angle
        aperture = camera.aperture

        photons = (
            b_fluxes
            * aperture
            * self.exposure.duration
            * camera.efficiency
            * solid_angle
        ).decompose()

        xx, yy = np.mgrid[0 : camera.num_pix[0], 0 : camera.num_pix[1]]
        xx = xx.reshape(-1, 1)
        yy = yy.reshape(-1, 1)
        inter_coords = np.concatenate((xx, yy), axis=1) + 0.5

        moon_image = griddata(b_coords_pix, photons, inter_coords).reshape(
            camera.num_pix
        )

        return moon_image

    ## @Martin Hein
    def simulate_bodyShape(self, scienceCamera, exposure, camera_frame, skycamera_frame):
        """
        Constructs shape of the camera body.

        The vertex positions are properties of scienceCamera.

        Calculates the coordinates of the pixel within the verticies and sets them to intensity from scienceCamera.

        Returns an image array of shape of exposure.image with all pixels and intensities.
        """
        # TODO: self.exposure ?

        # read from camera and transform to pixel coordinates
        vertex_positions_pix = transform_scienceCamera_to_exposurePixels(
            scienceCamera.vertex_positions, 
            camera_frame, 
            skycamera_frame, 
            exposure
        )

        # rasterize the polygon from vertex positions -> get coordinates of filled pixel
        coords_pix = np.array(rasterize_polygon(vertex_positions_pix))
        
        # same intensity over complete surface
        # TODO: intensity could be non-homogeneous, i.e. an array
        intensity = np.tile(scienceCamera.lighted_intensity, np.max(coords_pix.shape))
        
        # TODO: the following is also 1:1 used in line about 238 -> should be a single function
        nx, ny = exposure.camera.num_pix
        bodyShape, _ = np.histogramdd(
            coords_pix.swapaxes(0,1), # needed shape: (number_of_points, 2)
            bins=(nx, ny),
            weights=intensity, # shape: (number_of_points,)
            range=((-0.5, nx - 0.5), (-0.5, ny - 0.5)),
        )

        return bodyShape

    ## @Martin Hein
    def simulate_gaussianProfile(self, positions, radius, n_rays, intensity, exposure, camera_frame, skycamera_frame):
        """
        Simulates a gaussian profile for the led intensities by distributing ray positions by gaussian function.
        """

        # TODO:
        # Improvements:
        # - change axes from beginning, so no swapping axes will be needed
        # - "parallelize" transformation for all rays in some form of numpy operation
        # - use self.exposure ?
        
        n_spots = len(positions)

        # use 2D Gaussian (polar coordinates) as smearing kernel
        radial_offset = (
            np.sqrt(
                np.random.exponential(
                    radius.to_value(u.m) ** 2,
                    size=(n_rays, n_spots),
                )
            )
            * u.m
        )
        position_angle = (
            np.random.random(size=(n_rays, n_spots))
            * 360
            * u.deg
        )

        # polar to cartesian coordinates
        x_coords = radial_offset * np.cos(position_angle) # shape: (n_rays, n_spots)
        y_coords = radial_offset * np.sin(position_angle)

        coords_smeared = np.array([y_coords, x_coords]) # not really uesd except for creating coords_pix
        # shape: coords_smeared = [ y[rays[leds]], x[rays[leds]] ] = (2, n_rays, n_spots)

        coords_pix = np.zeros(coords_smeared.shape) # shape (2, n_rays, n_spots)

        # transformation from science camera coordinates to pixel coordinates
        # calculate for each ray individually
        for ray in range(n_rays):
            coords_per_ray = np.array([y_coords[ray], x_coords[ray]]) * u.m # (2, n_spots)

            ## need shape (n_spots, 2) for transformation, (2, n_spots) is given -> swapaxes
            swaped = np.swapaxes(coords_per_ray, 0,1) # (n_spots, 2)

            # add the position distribution to the positions
            swaped += positions

            # actual transformation
            # TODO: wait for direct tranformation to pixel coords
            coords_pix_per_ray = transform_scienceCamera_to_exposurePixels(swaped, camera_frame, skycamera_frame, exposure)
            
            # back to shape (2, n_spots) (because of the shape of coords_pix)
            coords_pix_per_ray = np.swapaxes(coords_pix_per_ray, 0,1) # (2, n_spots)

            # write pixel coordinates to array by same shape as before
            coords_pix[0][ray] = coords_pix_per_ray[0]
            coords_pix[1][ray] = coords_pix_per_ray[1]

        # distribute intensities over all rays
        distributed_intensity = intensity / n_rays
        distributed_intensity = np.tile(distributed_intensity, n_rays)

        return coords_pix, distributed_intensity

    ## @Martin Hein
    def simulate_LEDs(self, scienceCamera, exposure, camera_frame, skycamera_frame):
        """
        Constructs the LED positions and intensity distributions (gaussian) for each LED as property of scienceCamera.

        Returns an image array of shape of exposure.image with the constructed LED intensites.
        """
        # TODO: use self.exposure ?

        # calculate the pixel coordinates for the led intensities with gaussian profile
        coords_pix, distributed_intensity = self.simulate_gaussianProfile(
            scienceCamera.led_positions, 
            scienceCamera.led_radius, 
            scienceCamera.n_rays,
            scienceCamera.led_intensity,
            exposure,
            camera_frame, 
            skycamera_frame
        )

        # TODO: the following is also 1:1 used in line about 238 -> should be a single function
        # write the coordinates with intensites into a histogramm
        # which will lateer be overdrawn over camera body and exposure
        nx, ny = exposure.camera.num_pix
        leds, _ = np.histogramdd(
            coords_pix.swapaxes(0,2).reshape((-1,2)), # bring to right shape: (n_rays * n_spots, 2)
            bins=(nx, ny),
            weights=distributed_intensity, # shape: (n_rays * n_spots,)
            range=((-0.5, nx - 0.5), (-0.5, ny - 0.5)),
        )

        # return the resulting image
        return leds

    ## @Martin Hein
    def simulate_cameraBody(self, scienceCamera, exposure, exposureImage):
        """
        Constructs the body of the science camera.

        Body geometry and led position as well as the intensity of the lit body 
        and of the emitting LEDs are properties of scienceCamera.

        This function overwrites the exposure image with the generated body.
        """
        # TODO: exposure not as parameter -> self.exposure ?
        
        # precalculate camera frame and skycamera frame
        # in coordinate system of the Cherenkov camera
        camera_frame = CameraFrame(
            focal_length=scienceCamera.focal_length,
            rotation=scienceCamera.rotation
        )
        # in coordinate system of the chip
        skycamera_frame = SkyCameraFrame(
            focal_length=exposure.camera.focal_length[0],
            rotation=exposure.camera.rotation,
            tilt_x=exposure.camera.tilt[0],
            tilt_y=exposure.camera.tilt[1],
        )

        # create filled body shape with intensities
        body = self.simulate_bodyShape(scienceCamera, exposure, camera_frame, skycamera_frame)

        # simulate LEDs
        leds = self.simulate_LEDs(scienceCamera, exposure, camera_frame, skycamera_frame)

        # add LEDs onto the body
        body += leds

        # overwrite all pixel positions of image with the nonzero values of body
        x_coords, y_coords = np.where(body > 0)
        exposureImage[x_coords, y_coords] = body[x_coords, y_coords]

        # since the exposureImage instance gets changed, no return is needed
        # return exposureImage

## @Martin Hein
## could be moved to utils
def rasterize_polygon(vertex_positions_2D):
    """
    Calculates and returns the index positions of the pixels within the polygon verticies.
    """
    return polygon(vertex_positions_2D[:,0], vertex_positions_2D[:,1])

## @Martin Hein
## maybe move to ScienceCamera class?
# TODO: implement direct transformation from science camera to pixel
def transform_scienceCamera_to_exposurePixels(coordinates_sciCam, camera_frame, skycamera_frame, exposure):
    """
    Performs a transformation of coordinates in the space of the science camera to pixel coordinates of the chip.
    """
    positions = SkyCoord(
        coordinates_sciCam[:,0],
        coordinates_sciCam[:,1],
        frame=camera_frame
    )

    positions_pix = positions.transform_to(skycamera_frame)
    positions_pix = exposure.camera.transform_to_pixels(positions_pix)

    return positions_pix

## @Martin Hein
## instead of gaussian profile a uniform box with size of kernel is created
# -> delete if not needed
def simulate_boxProfile(exposure, box_kernel : tuple, led_positions_pixel, scienceCamera):
    leds_on_exposure = np.zeros(np.shape(exposure.image), dtype=exposure.image.dtype)
    
    for idx, center_pos in enumerate(led_positions_pixel):
        for i in range(box_kernel[0]):
            for j in range(box_kernel[1]):
                leds_on_exposure[
                    int(center_pos[0]) + i - box_kernel[0]//2, 
                    int(center_pos[1]) + j - box_kernel[1]//2
                ] = scienceCamera.led_peakIntensity[idx] # select corresponding intensity

    return leds_on_exposure